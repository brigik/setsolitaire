#include "Card.h"
#include "Deck.h"
#include "Set.h"
#include "Board.h"
#include "Dealer.h"
#include <iostream>
using namespace std;

int main()
{
	Deck *Francois = new Deck();
	Board *Gerhart = new Board();
	Dealer *Johnny = new Dealer();
	Set *set = new Set();

	Card card(Card::Number::ONE, Card::Symbol::OVAL, Card::Shading::SOLID, Card::Color::GREEN);
	Card card1(Card::Number::TWO, Card::Symbol::DIAMOND, Card::Shading::OPEN, Card::Color::RED);
	Card card2(Card::Number::THREE, Card::Symbol::SQUIGGLE, Card::Shading::STRIPED, Card::Color::BLUE);
	
	set->findSets(card, card1, card2);

	return 0;
}