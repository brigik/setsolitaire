#pragma once
#include "Card.h"
#include <iostream>
#include <vector>


class Set
{

public:
	Set();
	void findSets(const Card& card1, const Card& card2, const Card& card3) const;
	bool validateSets(const int& card1Position, const int& card2Position, const int& card3Position) const;
	~Set();

private:
	std::vector<Card> m_cardArray;
};

