#include "Colors.h"
#include <iostream>
#include <random>
#include <cstdlib>
#include <ctime>
using namespace std;

Colors generateRandomColor()
{
	Colors color;
	int randomNumber1 = rand() % 255 + 1;
	int randomNumber2 = rand() % 255 + 1;
	int randomNumber3 = rand() % 255 + 1;

	color.setRed(randomNumber1);
	color.setGreen(randomNumber2);
	color.setBlue(randomNumber3);

	return color;
}


/*int main()
{
	srand(time(NULL));
	cout << "Three RGB random colors: " << endl;
	cout << "Random color1: " << generateRandomColor() << endl;
	cout << "Random color2: " << generateRandomColor() << endl;
	cout << "Random color3: " << generateRandomColor() << endl;

	return 0;
}*/