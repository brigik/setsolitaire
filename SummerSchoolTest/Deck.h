#pragma once
#include <iostream>
#include <vector>
#include "Card.h"

class Deck
{

public:
	Deck();

	~Deck();

	void shuffleDeck();

	void swap(Card & firstCard, Card & secondCard);

	void printDeck() const;

	size_t getNumberOfCards();

	Card dealCard();

private:
	size_t m_numberOfCardsInDeck;
	std::vector<Card> m_cardArray;
	size_t m_index = 0;
};

