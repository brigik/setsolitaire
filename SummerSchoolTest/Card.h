#pragma once
#include <iostream>
class Card
{

public:
	enum class Number
	{
		ONE = 1,
		TWO,
		THREE
	};

	enum class Symbol
	{
		DIAMOND = 'D',
		SQUIGGLE = 'S',
		OVAL = 'O'
	};

	enum class Shading
	{
		SOLID = 'S',
		STRIPED = 's',
		OPEN = 'O'
	};

	enum class Color
	{
		RED = 'R',
		GREEN = 'G',
		BLUE = 'B'
	};

	Card();

	Card(const Number& number, const Symbol& symbol, const Shading& shading, const Color& color);

	Card(const Card& other);

	static std::string numberToString(const Number& number);

	void printCard() const;

	friend std::ostream& operator << (std::ostream& o, const Card& card);

	int getNumber() const;

	char getSymbol() const;

	char getShading() const;

	char getColor() const;

	~Card();

private:
	Number m_number;
	Symbol m_symbol;
	Shading m_shading;
	Color m_color;
};

