#include "Set.h"
#include "Card.h"
/*
A set consists of three cards which satisfy all of these conditions:
- they have the same number or they have three different numbers
- they have the same symbol or they have three different symbols
- they have the same shading or they have three different shadings
- they have the same color or they have three different colors
*/

Set::Set()
{
}

bool areCardNumbersDifferent(const Card& card1, const Card& card2, const Card& card3)
{
	return card1.getNumber() != card2.getNumber() &&
		card1.getNumber() != card3.getNumber() &&
		card3.getNumber() != card2.getNumber();
}

bool areCardNumbersEqual(const Card& card1, const Card& card2, const Card& card3)
{
	return card1.getNumber() == card2.getNumber() &&
		card1.getNumber() == card3.getNumber() &&
		card3.getNumber() == card2.getNumber();
}

bool areCardSymbolsDifferent(const Card& card1, const Card& card2, const Card& card3)
{
	return card1.getSymbol() != card2.getSymbol() &&
		card1.getSymbol() != card3.getSymbol() &&
		card3.getSymbol() != card2.getSymbol();
}

bool areCardSymbolsEqual(const Card& card1, const Card& card2, const Card& card3)
{
	return card1.getSymbol() == card2.getSymbol() && 
		card1.getSymbol() == card3.getSymbol() && 
		card3.getSymbol() == card2.getSymbol();
	
}

bool areCardShadingsDifferent(const Card& card1, const Card& card2, const Card& card3)
{
	return card1.getShading() != card2.getShading() && 
		card1.getShading() != card3.getShading() && 
		card3.getShading() != card2.getShading();
}

bool areCardShadingsEqual(const Card& card1, const Card& card2, const Card& card3)
{
	return card1.getShading() == card2.getShading() && 
		card1.getShading() == card3.getShading() && 
		card3.getShading() == card2.getShading();
}

bool areCardColoursDifferent(const Card& card1, const Card& card2, const Card& card3)
{
	return card1.getColor() != card2.getColor() && 
		card1.getColor() != card3.getColor() && 
		card3.getColor() != card2.getColor();
}

bool areCardColoursEqual(const Card& card1, const Card& card2, const Card& card3)
{
	return card1.getColor() == card2.getColor() && card1.getColor() == card3.getColor() && card3.getColor() == card2.getColor();
}

bool areAllCardsEqual(const Card& card1, const Card& card2, const Card& card3)
{
	return areCardNumbersEqual(card1, card2, card3) &&
		areCardSymbolsEqual(card1, card2, card3) &&
		areCardShadingsEqual(card1, card2, card3) &&
		areCardColoursEqual(card1, card2, card3);
}

bool areAllCardsDifferent(const Card& card1, const Card& card2, const Card& card3)
{
	return areCardNumbersDifferent(card1, card2, card3) &&
		areCardSymbolsDifferent(card1, card2, card3) &&
		areCardShadingsDifferent(card1, card2, card3) &&
		areCardColoursDifferent(card1, card2, card3);
}

void Set::findSets(const Card& card1, const Card& card2, const Card& card3) const
{
	if (areAllCardsEqual(card1, card2, card3) || areAllCardsDifferent(card1, card2, card3))
	{
		std::cout << "The three cards form a set!" << std::endl;
	}
	else
	{
		std::cout << "The three cards do NOT form a set!" << std::endl;
	}
}

bool Set::validateSets(const int & card1Position, const int & card2Position, const int & card3Position) const
{
	return false;
}

Set::~Set()
{
}
